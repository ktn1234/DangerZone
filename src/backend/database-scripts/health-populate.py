import json
import os
import psycopg2


def main():
    cnx = ''
    try:
        connection = psycopg2.connect(user=os.environ['DB_USERNAME'],
                                      password=os.environ['DB_PASSWORD'],
                                      host=os.environ['DB_HOST'],
                                      port=os.environ['DB_PORT'],
                                      database=os.environ['DB_DATABASE'])

    except (Exception, psycopg2.Error) as error:
        print("ERROR: Could not connect to PostgreSQL DB")
        print("ERROR: " + str(error))
        raise error

    with open("../assets/health.json", "r") as health_json:
        data = health_json.read()

    health_city = json.loads(data)

    cur = connection.cursor()
    for health in health_city:
        health['city'] = str(health['city']).replace("'", "''")
        select_stmt = ("SELECT id FROM cities WHERE city=\'" + health['city'] + "\'")

        cur.execute(select_stmt)
        id_result = cur.fetchone()[0]

        city_id = id_result

        air_pollution_particle = health['air_pollution_particle']
        obesity = health['obesity']
        frequent_mental_distress = health['frequent_mental_distress']
        smoking = health['smoking']
        diabetes = health['diabetes']
        walkability = health['walkability']
        opioid_overdose_deaths = health['opioid_overdose_deaths']
        life_expectancy = health['life_expectancy']
        limited_access_to_healthy_foods = health['limited_access_to_healthy_foods']
        cardiovascular_disease_deaths = health['cardiovascular_disease_deaths']
        lead_exposure_risk_index = health['lead_exposure_risk_index']

        try:

            # Prepared statement so no sql injections on incur
            insert_stmt = (
                "INSERT INTO health (city_id, air_pollution_particle, obesity, frequent_mental_distress, smoking, "
                "diabetes, walkability, opioid_overdose_deaths, life_expectancy, limited_access_to_healthy_foods, "
                "cardiovascular_disease_deaths, lead_exposure_risk_index) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, "
                "%s, %s, %s) "
            )

            data = (
                city_id, air_pollution_particle, obesity, frequent_mental_distress, smoking, diabetes, walkability,
                opioid_overdose_deaths, life_expectancy, limited_access_to_healthy_foods,
                cardiovascular_disease_deaths, lead_exposure_risk_index
            )

            cur.execute(insert_stmt, data)

        except (Exception, psycopg2.Error) as error:
            print("ERROR: Could not fulfill POST request")
            print("ERROR: " + str(error))
            connection.close()
            raise error

        connection.commit()
        print("POST request: Successfully executed command.")

    connection.close()


if __name__ == '__main__':
    main()
