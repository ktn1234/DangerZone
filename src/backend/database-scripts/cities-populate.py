import json
import os
import psycopg2


def main():
    cnx = ''
    try:
        connection = psycopg2.connect(user=os.environ['DB_USERNAME'],
                                      password=os.environ['DB_PASSWORD'],
                                      host=os.environ['DB_HOST'],
                                      port=os.environ['DB_PORT'],
                                      database=os.environ['DB_DATABASE'])

    except (Exception, psycopg2.Error) as error:
        print("ERROR: Could not connect to PostgreSQL DB")
        print("ERROR: " + str(error))
        raise error

    with open("../assets/cities.json", "r") as cities_json:
        data = cities_json.read()

    cities = json.loads(data)
    for city in cities:
        _city = city['city']
        state = city['state']
        population = city['population']
        latitude = city['latitude']
        longitude = city['longitude']
        time_zone = city['time_zone']
        total_crime_incidents = city['total_crime_incidents']
        average_high = city['average_high']
        average_low = city['average_low']
        life_expectancy = city['life_expectancy']
        average_ozone = city['average_ozone']

        try:

            # Prepared statement so no sql injections on incur
            insert_stmt = (
                "INSERT INTO cities (city, state, population, latitude, longitude, time_zone, total_crime_incidents, "
                "average_high, average_low, life_expectancy, average_ozone) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, "
                "%s, %s, %s)"
            )

            data = (
            _city, state, population, latitude, longitude, time_zone, total_crime_incidents, average_high, average_low,
            life_expectancy, average_ozone)

            cur = connection.cursor()
            cur.execute(insert_stmt, data)

        except (Exception, psycopg2.Error) as error:
            print("ERROR: Could not fulfill POST request")
            print("ERROR: " + str(error))
            connection.close()
            raise error

        connection.commit()
        print("POST request: Successfully executed command.")

    connection.close()


if __name__ == '__main__':
    main()
