import json
import os
import psycopg2


def main():
    cnx = ''
    try:
        connection = psycopg2.connect(user=os.environ['DB_USERNAME'],
                                      password=os.environ['DB_PASSWORD'],
                                      host=os.environ['DB_HOST'],
                                      port=os.environ['DB_PORT'],
                                      database=os.environ['DB_DATABASE'])

    except (Exception, psycopg2.Error) as error:
        print("ERROR: Could not connect to PostgreSQL DB")
        print("ERROR: " + str(error))
        raise error

    with open("../assets/climate.json", "r") as climate_json:
        data = climate_json.read()

    climates = json.loads(data)

    cur = connection.cursor()
    for climate in climates:
        climate['city'] = str(climate['city']).replace("'", "''")
        select_stmt = ("SELECT id FROM cities WHERE city='" + climate['city'] + "'")
        cur.execute(select_stmt)
        id_result = cur.fetchone()[0]

        city_id = id_result
        average_ozone = climate['average_ozone']

        spring_average_temp = climate['spring']['temperature']
        spring_wind_speed = climate['spring']['windSpeed']
        spring_humidity = climate['spring']['humidity']
        spring_uv = climate['spring']['uv']
        spring_ozone = climate['spring']['ozone']

        summer_average_temp = climate['summer']['temperature']
        summer_wind_speed = climate['summer']['windSpeed']
        summer_humidity = climate['summer']['humidity']
        summer_uv = climate['summer']['uv']
        summer_ozone = climate['summer']['ozone']

        fall_average_temp = climate['fall']['temperature']
        fall_wind_speed = climate['fall']['windSpeed']
        fall_humidity = climate['fall']['humidity']
        fall_uv = climate['fall']['uv']
        fall_ozone = climate['fall']['ozone']

        winter_average_temp = climate['winter']['temperature']
        winter_wind_speed = climate['winter']['windSpeed']
        winter_humidity = climate['winter']['humidity']
        winter_uv = climate['winter']['uv']
        winter_ozone = climate['winter']['ozone']

        try:

            # Prepared statement so no sql injections on incur
            insert_stmt = (
                "INSERT INTO climate (city_id, average_ozone, spring_average_temp, spring_wind_speed, "
                "spring_humidity, spring_uv, spring_ozone, summer_average_temp, summer_wind_speed, summer_humidity, "
                "summer_uv, summer_ozone, fall_average_temp, fall_wind_speed, fall_humidity, fall_uv, fall_ozone, "
                "winter_average_temp, winter_wind_speed, winter_humidity, winter_uv, winter_ozone) VALUES (%s, %s, "
                "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) "
            )

            data = (
                city_id, average_ozone, spring_average_temp, spring_wind_speed, spring_humidity, spring_uv, spring_ozone, summer_average_temp,
                summer_wind_speed, summer_humidity, summer_uv, summer_ozone, fall_average_temp, fall_wind_speed, fall_humidity, fall_uv, fall_ozone, winter_average_temp,
                winter_wind_speed, winter_humidity, winter_uv, winter_ozone
            )

            cur.execute(insert_stmt, data)

        except (Exception, psycopg2.Error) as error:
            print("ERROR: Could not fulfill POST request")
            print("ERROR: " + str(error))
            connection.close()
            raise error

        connection.commit()
        print("POST request: Successfully executed command.")

    connection.close()


if __name__ == '__main__':
    main()
