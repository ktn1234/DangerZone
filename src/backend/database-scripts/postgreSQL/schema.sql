-- Drops the dangerzone if it exists currently --
DROP DATABASE IF EXISTS dangerzone;

-- Creates the "dangerzone" database --
CREATE DATABASE dangerzone;

\c dangerzone;
\password

CREATE TABLE cities(
  id BIGSERIAL PRIMARY KEY NOT NULL,
  city VARCHAR(30) NOT NULL,
  state VARCHAR(30) NOT NULL,
  population BIGINT NOT NULL,
  time_zone VARCHAR(30) NOT NULL,
  latitude FLOAT(10) NOT NULL,
  longitude FLOAT(10) NOT NULL,
  average_high BIGINT NOT NULL,
  average_low BIGINT NOT NULL,
  average_ozone BIGINT NOT NULL,
  life_expectancy FLOAT(10) NOT NULL,
  total_crime_incidents BIGINT NOT NULL
);

CREATE TABLE crimes(
  id BIGSERIAL PRIMARY KEY NOT NULL,
  city_id BIGINT REFERENCES cities(id),
  total_crime_incidents BIGINT NOT NULL,
  murder BIGINT NOT NULL, -- Murder & Non-negligent Manslaughter
  assault BIGINT NOT NULL, -- Aggravated Assault
  drugs BIGINT NOT NULL, -- Drug/Narcotic Violations
  burglary BIGINT NOT NULL, -- Burglary/Breaking & Entering
  vandalism BIGINT NOT NULL, -- Destruction/Damage/Vandalism of Property
  dui BIGINT NOT NULL, -- Driving Under the Influence
  sex_offenses BIGINT NOT NULL, -- Sex Offense/All Other
  theft BIGINT NOT NULL, -- Theft From Building
  animal_cruelty BIGINT NOT NULL, -- Animal Cruelty
  other_offenses BIGINT NOT NULL -- All Other Offenses
);

CREATE TABLE health(
  id BIGSERIAL PRIMARY KEY NOT NULL,
  city_id BIGINT REFERENCES cities(id),
  air_pollution_particle FLOAT(10) NOT NULL,
  cardiovascular_disease_deaths FLOAT(10) NOT NULL,
  frequent_mental_distress FLOAT(10) NOT NULL,
  lead_exposure_risk_index FLOAT(10) NOT NULL,
  opioid_overdose_deaths FLOAT(10) NOT NULL,
  smoking FLOAT(10) NOT NULL,
  diabetes FLOAT(10) NOT NULL,
  obesity FLOAT(10) NOT NULL,
  life_expectancy FLOAT(10) NOT NULL,
  limited_access_to_healthy_foods FLOAT(10) NOT NULL,
  walkability FLOAT(10) NOT NULL
);

CREATE TABLE climate(
  id BIGSERIAL PRIMARY KEY NOT NULL,
  city_id BIGINT REFERENCES cities(id),
  average_ozone FLOAT(30) NOT NULL,
  spring_average_temp FLOAT(30) NOT NULL,
  spring_wind_speed FLOAT(30) NOT NULL,
  spring_humidity FLOAT(30) NOT NULL,
  spring_uv FLOAT(30) NOT NULL,
  spring_ozone FLOAT(30) NOT NULL,
  summer_average_temp FLOAT(30) NOT NULL,
  summer_wind_speed FLOAT(30) NOT NULL,
  summer_humidity FLOAT(30) NOT NULL,
  summer_uv FLOAT(30) NOT NULL,
  summer_ozone FLOAT(30) NOT NULL,
  fall_average_temp FLOAT(30) NOT NULL,
  fall_wind_speed FLOAT(30) NOT NULL,
  fall_humidity FLOAT(30) NOT NULL,
  fall_uv FLOAT(30) NOT NULL,
  fall_ozone FLOAT(30) NOT NULL,
  winter_average_temp FLOAT(30) NOT NULL,
  winter_wind_speed FLOAT(30) NOT NULL,
  winter_humidity FLOAT(30) NOT NULL,
  winter_uv FLOAT(30) NOT NULL,
  winter_ozone FLOAT(30) NOT NULL
);

--Command for PostgreSQL schema: \d+ cities;