# Services - Business Logic
# Data Access Object - Database Query Logic
# Tightly coupled in this case
import sqlalchemy
from flask import abort
import sys

from sqlalchemy import Table, select, column

from .schemas import Base, Session, Crime


class CrimeService:
    def __init__(self, engine):
        self.engine = engine

    def get_all_cities_crimes(self):
        connection = self.engine.connect()

        table = Table('climate', Base.metadata, autoload=True)
        stmt = select([table])
        query_result = connection.execute(stmt)

        crimes = []
        for row in query_result:
            crime = {
                'city': self.get_corresponding_city(row[1]),
                'total_crime_incidents': row[2],
                'murder': row[3],
                'assault': row[4],
                'drugs': row[5],
                'burglary': row[6],
                'vandalism': row[7],
                'dui': row[8],
                'sex_offenses': row[9],
                'theft': row[10],
                'animal_cruelty': row[11],
                'other_offenses': row[12]
            }
            crimes.append(crime)
        connection.close()

        return crimes
    def get_all_cities_crime_paginate(self, offset, limit):
        session = Session()
        query_result = session.query(Crime).limit(limit).offset(offset).all()
        cities = []
        for row in query_result:
            crime = {
                'city': self.get_corresponding_city(row.city_id),
                'total_crime_incidents': row.total_crime_incidents,
                'murder': row.murder,
                'assault': row.assault,
                'drugs': row.drugs,
                'burglary': row.burglary,
                'vandalism': row.vandalism,
                'dui': row.dui,
                'sex_offenses': row.sex_offenses,
                'theft': row.theft,
                'animal_cruelty': row.animal_cruelty,
                'other_offenses': row.other_offenses
            }
            cities.append(crime)
        session.close()

        return cities

    def get_all_cities_crime_sort_paginate(self, column, asc, offset, limit):
        session = Session()

        order = None
        if(asc.lower() == 'true'):
            order = sqlalchemy.asc
        else:
            order = sqlalchemy.desc

        query_result = session.query(Crime).order_by(order(Crime.__dict__[column])).limit(limit).offset(offset).all()
        cities = []
        for row in query_result:
            crime = {
                'city': self.get_corresponding_city(row.city_id),
                'total_crime_incidents': row.total_crime_incidents,
                'murder': row.murder,
                'assault': row.assault,
                'drugs': row.drugs,
                'burglary': row.burglary,
                'vandalism': row.vandalism,
                'dui': row.dui,
                'sex_offenses': row.sex_offenses,
                'theft': row.theft,
                'animal_cruelty': row.animal_cruelty,
                'other_offenses': row.other_offenses
            }
            cities.append(crime)
        session.close()

        return cities


    def get_city_crime(self, param_value):
        connection = self.engine.connect()

        table = Table('crimes', Base.metadata, autoload=True)
        city_id = self.get_corresponding_city_id(param_value)
        stmt = select([table]).where(table.c.city_id == city_id)

        try:
            query_result = connection.execute(stmt)
            if query_result is None:
                connection.close()
                abort(404)

            crime = []
            for row in query_result:
                sanitize_data = {
                    'city': param_value,
                    'total_crime_incidents': row[2],
                    'murder': row[3],
                    'assault': row[4],
                    'drugs': row[5],
                    'burglary': row[6],
                    'vandalism': row[7],
                    'dui': row[8],
                    'sex_offenses': row[9],
                    'theft': row[10],
                    'animal_cruelty': row[11],
                    'other_offenses': row[12]
                }
                crime.append(sanitize_data)
            connection.close()

            return crime
        except Exception as error:
            print(error)
            connection.close()
            abort(400)

    def get_city_crime_ids(self, crime_ids):
        session = Session()

        try:
            query_result = session.query(Crime).filter(Crime.id.in_(crime_ids)).all()
            if query_result is None:
                session.close()
                abort(404)

            crime = []
            for row in query_result:
                sanitize_data = {
                    'city': self.get_corresponding_city(row.city_id),
                    'total_crime_incidents': row.total_crime_incidents,
                    'murder': row.murder,
                    'assault': row.assault,
                    'drugs': row.drugs,
                    'burglary': row.burglary,
                    'vandalism': row.vandalism,
                    'dui': row.dui,
                    'sex_offenses': row.sex_offenses,
                    'theft': row.theft,
                    'animal_cruelty': row.animal_cruelty,
                    'other_offenses': row.other_offenses
                }
                crime.append(sanitize_data)
            session.close()

            return crime
        except Exception as error:
            print(error)
            session.close()
            abort(400)

    def get_worst_three_city_crimes(self, param_value):
        connection = self.engine.connect()

        table = Table('crimes', Base.metadata, autoload=True)
        city_id = self.get_corresponding_city_id(param_value)
        stmt = select([table]).where(table.c.city_id == city_id)

        try:
            query_result = connection.execute(stmt)

            one_row = None
            for row in query_result:
                one_row = row


            if one_row is None:
                connection.close()
                abort(404)

            crime = []
            city = self.get_corresponding_city(one_row[1]),
            sanitize_data = {
                'murder': one_row[3],
                'assault': one_row[4],
                'drugs': one_row[5],
                'burglary': one_row[6],
                'vandalism': one_row[7],
                'dui': one_row[8],
                'sex_offenses': one_row[9],
                'theft': one_row[10],
                'animal_cruelty': one_row[11]
            }

            keys = iter(sanitize_data)
            third = first = second = -sys.maxsize
            third_key = first_key = second_key = ""
            for k in keys:
                if sanitize_data[k] > first:
                    third = second
                    second = first
                    first = sanitize_data[k]
                    third_key = second_key
                    second_key = first_key
                    first_key = k
                elif sanitize_data[k] > second:
                    third = second
                    second = sanitize_data[k]
                    third_key = second_key
                    second_key = k
                elif sanitize_data[k] > third:
                    third = sanitize_data[k]
                    third_key = k

            # first, second, third worst crimes
            print(first_key + " " + second_key + " " + third_key)
            print(str(first) + " " + str(second) + " " + str(third))

            crime.append({
                'city': city[0],
                first_key: first,
                second_key: second,
                third_key: third
            })
            connection.close()

            return crime
        except Exception as error:
            print(error)
            connection.close()
            abort(400)

    def get_city_crime_by_filter(self, attribute, param_value):
        connection = self.engine.connect()

        table = Table('crimes', Base.metadata, autoload=True)

        condition = attribute.split("_")[-1]
        stmt = None
        if condition == 'lt':
            attribute = attribute[0:len(attribute) - 3]

            stmt = select([table]).where(table.c[attribute] < param_value)
        if condition == 'gt':
            attribute = attribute[0:len(attribute) - 3]

            stmt = select([table]).where(table.c[attribute] > param_value)

        print(attribute)
        print(stmt)

        try:
            query_result = connection.execute(stmt)

            crimes = []
            for row in query_result:
                crime = {
                    'city': self.get_corresponding_city(row[1]),
                    'total_crime_incidents': row[2],
                    'murder': row[3],
                    'assault': row[4],
                    'drugs': row[5],
                    'burglary': row[6],
                    'vandalism': row[7],
                    'dui': row[8],
                    'sex_offenses': row[9],
                    'theft': row[10],
                    'animal_cruelty': row[11],
                    'other_offenses': row[12]
                }
                crimes.append(crime)
            connection.close()

            return crimes
        except Exception as error:
            print(error)
            connection.close()

            abort(400)

    def get_corresponding_city_id(self, city_name):
        try:
            connection = self.engine.connect()

            table = Table('cities', Base.metadata, autoload=True)
            stmt = select([table]).where(column('city') == city_name)
            query_result = connection.execute(stmt)
            city = None
            for row in query_result:
                city = row[0]
            connection.close()

            return city
        except Exception as error:
            connection.close()

            print(error)
            abort(404)

    def get_corresponding_city(self, city_id):
        try:
            connection = self.engine.connect()

            table = Table('cities', Base.metadata, autoload=True)
            stmt = select([table]).where(column('id') == city_id)
            query_result = connection.execute(stmt)

            city = None
            for row in query_result:
                city = row[1]
            connection.close()

            return city
        except Exception as error:
            print(error)
            connection.close()

            abort(404)
