# Services - Business Logic
# Data Access Object - Database Query Logic
# Tightly coupled in this case
import sqlalchemy
from flask import abort
from sqlalchemy import Table, select, column

from .schemas import Base, Session, Climate


class ClimateService:
    def __init__(self, engine):
        self.engine = engine

    def get_all_cities_climate(self):
        connection = self.engine.connect()

        table = Table('climate', Base.metadata, autoload=True)
        stmt = select([table])
        query_result = connection.execute(stmt)

        climates = []
        for row in query_result:
            climate = {
                'city': self.get_corresponding_city(row[1]),
                'average_ozone': row[2],
                'spring': {
                    "temperature": row[3],
                    "windSpeed": row[4],
                    "humidity": row[5],
                    "uv": row[6],
                    "ozone": row[7]
                },
                "summer": {
                    "temperature": row[8],
                    "windSpeed": row[9],
                    "humidity": row[10],
                    "uv": row[11],
                    "ozone": row[12]
                },
                "fall": {
                    "temperature": row[13],
                    "windSpeed": row[14],
                    "humidity": row[15],
                    "uv": row[16],
                    "ozone": row[17]
                },
                "winter": {
                    "temperature": row[18],
                    "windSpeed": row[19],
                    "humidity": row[20],
                    "uv": row[21],
                    "ozone": row[22]
                },
            }
            climates.append(climate)
        connection.close()

        return climates

    def get_all_cities_climate_paginate(self, offset, limit):
        session = Session()
        query_result = session.query(Climate).limit(limit).offset(offset).all()
        cities = []
        for row in query_result:
            climate = {
                'city': self.get_corresponding_city(row.city_id),
                'average_ozone': row.average_ozone,
                'spring': {
                    "temperature": row.spring_average_temp,
                    "windSpeed": row.spring_wind_speed,
                    "humidity": row.spring_humidity,
                    "uv": row.spring_uv,
                    "ozone": row.spring_ozone
                },
                "summer": {
                    "temperature": row.summer_average_temp,
                    "windSpeed": row.summer_wind_speed,
                    "humidity": row.summer_humidity,
                    "uv": row.summer_uv,
                    "ozone": row.summer_ozone
                },
                "fall": {
                    "temperature": row.fall_average_temp,
                    "windSpeed": row.fall_wind_speed,
                    "humidity": row.fall_humidity,
                    "uv": row.fall_uv,
                    "ozone": row.fall_ozone
                },
                "winter": {
                    "temperature": row.winter_average_temp,
                    "windSpeed": row.winter_wind_speed,
                    "humidity": row.winter_humidity,
                    "uv": row.winter_uv,
                    "ozone": row.winter_ozone
                },
            }
            cities.append(climate)
        session.close()

        return cities

    def get_all_cities_climate_sort_paginate(self, column, asc, offset, limit):
        session = Session()

        order = None
        if(asc.lower() == 'true'):
            order = sqlalchemy.asc
        else:
            order = sqlalchemy.desc

        query_result = session.query(Climate).order_by(order(Climate.__dict__[column])).limit(limit).offset(offset).all()
        cities = []
        for row in query_result:
            climate = {
                'city': self.get_corresponding_city(row.city_id),
                'average_ozone': row.average_ozone,
                'spring': {
                    "temperature": row.spring_average_temp,
                    "windSpeed": row.spring_wind_speed,
                    "humidity": row.spring_humidity,
                    "uv": row.spring_uv,
                    "ozone": row.spring_ozone
                },
                "summer": {
                    "temperature": row.summer_average_temp,
                    "windSpeed": row.summer_wind_speed,
                    "humidity": row.summer_humidity,
                    "uv": row.summer_uv,
                    "ozone": row.summer_ozone
                },
                "fall": {
                    "temperature": row.fall_average_temp,
                    "windSpeed": row.fall_wind_speed,
                    "humidity": row.fall_humidity,
                    "uv": row.fall_uv,
                    "ozone": row.fall_ozone
                },
                "winter": {
                    "temperature": row.winter_average_temp,
                    "windSpeed": row.winter_wind_speed,
                    "humidity": row.winter_humidity,
                    "uv": row.winter_uv,
                    "ozone": row.winter_ozone
                },
            }
            cities.append(climate)
        session.close()

        return cities

    def get_climate_by_season(self, param_value):
        average_temp = param_value + "_average_temp"
        wind_speed = param_value + "_wind_speed"
        humidity = param_value + "_humidity"
        uv = param_value + "_uv"
        ozone = param_value + "_ozone"

        connection = self.engine.connect()

        table = Table('climate', Base.metadata, autoload=True)
        stmt = select([table.c.city_id,
                       table.c[average_temp],
                       table.c[wind_speed],
                       table.c[humidity],
                       table.c[uv],
                       table.c[ozone]])


        try:
            query_result = connection.execute(stmt)
            climates = []
            for row in query_result:
                climate = {
                    'city': self.get_corresponding_city(row[0]),
                    param_value: {
                        "temperature": row[1],
                        "windSpeed": row[2],
                        "humidity": row[3],
                        "uv": row[4],
                        "ozone": row[5]
                    }
                }
                climates.append(climate)
            connection.close()

            return climates
        except Exception as error:
            print(error)
            connection.close()
            abort(400)

    def get_city_climate_ids(self, climate_ids):
        session = Session()

        try:
            query_result = session.query(Climate).filter(Climate.id.in_(climate_ids)).all()
            if query_result is None:
                session.close()
                abort(404)

            cities = []
            for row in query_result:
                climate = {
                    'city': self.get_corresponding_city(row.city_id),
                    'average_ozone': row.average_ozone,
                    'spring': {
                        "temperature": row.spring_average_temp,
                        "windSpeed": row.spring_wind_speed,
                        "humidity": row.spring_humidity,
                        "uv": row.spring_uv,
                        "ozone": row.spring_ozone
                    },
                    "summer": {
                        "temperature": row.summer_average_temp,
                        "windSpeed": row.summer_wind_speed,
                        "humidity": row.summer_humidity,
                        "uv": row.summer_uv,
                        "ozone": row.summer_ozone
                    },
                    "fall": {
                        "temperature": row.fall_average_temp,
                        "windSpeed": row.fall_wind_speed,
                        "humidity": row.fall_humidity,
                        "uv": row.fall_uv,
                        "ozone": row.fall_ozone
                    },
                    "winter": {
                        "temperature": row.winter_average_temp,
                        "windSpeed": row.winter_wind_speed,
                        "humidity": row.winter_humidity,
                        "uv": row.winter_uv,
                        "ozone": row.winter_ozone
                    },
                }
                cities.append(climate)
            session.close()

            return cities
        except Exception as error:
            print(error)
            session.close()
            abort(400)

    def get_climate_by_city(self, param_value):
        connection = self.engine.connect()

        table = Table('climate', Base.metadata, autoload=True)
        city_id = self.get_corresponding_city_id(param_value)
        stmt = select([table]).where(table.c.city_id == city_id)

        try:
            query_result = connection.execute(stmt)
            if query_result is None:
                connection.close()

                abort(404)
            climates = []

            for row in query_result:
                climate = {
                    'city': self.get_corresponding_city(row[1]),
                    'average_ozone': row[2],
                    'spring': {
                        "temperature": row[3],
                        "windSpeed": row[4],
                        "humidity": row[5],
                        "uv": row[6],
                        "ozone": row[7]
                    },
                    "summer": {
                        "temperature": row[8],
                        "windSpeed": row[9],
                        "humidity": row[10],
                        "uv": row[11],
                        "ozone": row[12]
                    },
                    "fall": {
                        "temperature": row[13],
                        "windSpeed": row[14],
                        "humidity": row[15],
                        "uv": row[16],
                        "ozone": row[17]
                    },
                    "winter": {
                        "temperature": row[18],
                        "windSpeed": row[19],
                        "humidity": row[20],
                        "uv": row[21],
                        "ozone": row[22]
                    },
                }
                climates.append(climate)
            connection.close()

            return climates
        except Exception as error:
            print(error)
            connection.close()

            abort(400)

    def get_climate_by_city_and_season(self, attribute1, attribute2, param_value1, param_value2):
        city_val = ''
        season_val = ''

        if attribute1 == 'city':
            city_val = param_value1
        elif attribute1 == 'season':
            season_val = param_value1

        if attribute2 == 'city':
            city_val = param_value2
        elif attribute2 == 'season':
            season_val = param_value2

        if city_val == '' or season_val == '':
            abort(400)

        connection = self.engine.connect()

        table = Table('climate', Base.metadata, autoload=True)
        city_id = self.get_corresponding_city_id(city_val)
        stmt = select([table]).where(table.c.city_id == city_id)

        try:
            query_result = connection.execute(stmt)
            if query_result is None:
                connection.close()
                abort(404)

            for row in query_result:
                climate = {
                    'spring': {
                        "temperature": row[3],
                        "windSpeed": row[4],
                        "humidity": row[5],
                        "uv": row[6],
                        "ozone": row[7]
                    },
                    "summer": {
                        "temperature": row[8],
                        "windSpeed": row[9],
                        "humidity": row[10],
                        "uv": row[11],
                        "ozone": row[12]
                    },
                    "fall": {
                        "temperature": row[13],
                        "windSpeed": row[14],
                        "humidity": row[15],
                        "uv": row[16],
                        "ozone": row[17]
                    },
                    "winter": {
                        "temperature": row[18],
                        "windSpeed": row[19],
                        "humidity": row[20],
                        "uv": row[21],
                        "ozone": row[22]
                    },
                }

            city_season_climate = []
            res = {
                'city': city_val,
                season_val: climate[season_val]
            }

            city_season_climate.append(res)
            connection.close()

            return city_season_climate
        except Exception as error:
            print(error)
            connection.close()

            abort(400)

    def get_corresponding_city_id(self, city_name):
        try:
            connection = self.engine.connect()

            table = Table('cities', Base.metadata, autoload=True)
            stmt = select([table]).where(column('city') == city_name)
            query_result = connection.execute(stmt)
            city = None
            for row in query_result:
                city = row[0]
            connection.close()

            return city
        except Exception as error:
            print(error)
            connection.close()

            abort(404)

    def get_corresponding_city(self, city_id):
        try:
            connection = self.engine.connect()

            table = Table('cities', Base.metadata, autoload=True)
            stmt = select([table]).where(column('id') == city_id)
            query_result = connection.execute(stmt)

            city = None
            for row in query_result:
                city = row[1]
            connection.close()

            return city
        except Exception as error:
            print(error)
            connection.close()

            abort(404)
