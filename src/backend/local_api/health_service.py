# Services - Business Logic
# Data Access Object - Database Query Logic
# Tightly coupled in this case
import sqlalchemy
from flask import abort
from sqlalchemy import Table, select, column

from .schemas import Base, Session, Health


class HealthService:
    def __init__(self, engine):
        self.engine = engine

    def get_all_cities_health(self):
        connection = self.engine.connect()

        table = Table('health', Base.metadata, autoload=True)
        stmt = select([table])
        query_result = connection.execute(stmt)

        health_city = []
        for row in query_result:
            health = {
                'city': self.get_corresponding_city(row[1]),
                'air_pollution_particle': row[2],
                'cardiovascular_disease_deaths': row[3],
                'frequent_mental_distress': row[4],
                'lead_exposure_risk_index': row[5],
                'opioid_overdose_deaths': row[6],
                'smoking': row[7],
                'diabetes': row[8],
                'obesity': row[9],
                'life_expectancy': row[10],
                'limited_access_to_healthy_foods': row[11],
                'walkability': row[12]
            }
            health_city.append(health)
        connection.close()

        return health_city

    def get_all_cities_health_paginate(self, offset, limit):
        session = Session()
        query_result = session.query(Health).limit(limit).offset(offset).all()
        cities = []
        for row in query_result:
            city = {
                'city': self.get_corresponding_city(row.city_id),
                'air_pollution_particle': row.air_pollution_particle,
                'cardiovascular_disease_deaths': row.cardiovascular_disease_deaths,
                'frequent_mental_distress': row.frequent_mental_distress,
                'lead_exposure_risk_index': row.lead_exposure_risk_index,
                'opioid_overdose_deaths': row.opioid_overdose_deaths,
                'smoking': row.smoking,
                'diabetes': row.diabetes,
                'obesity': row.obesity,
                'life_expectancy': row.life_expectancy,
                'limited_access_to_healthy_foods': row.limited_access_to_healthy_foods,
                'walkability': row.walkability
            }
            cities.append(city)
        session.close()

        return cities

    def get_all_cities_health_sort_paginate(self, column, asc, offset, limit):
        session = Session()

        order = None
        if(asc.lower() == 'true'):
            order = sqlalchemy.asc
        else:
            order = sqlalchemy.desc

        query_result = session.query(Health).order_by(order(Health.__dict__[column])).limit(limit).offset(offset).all()
        cities = []
        for row in query_result:
            city = {
                'city': self.get_corresponding_city(row.city_id),
                'air_pollution_particle': row.air_pollution_particle,
                'cardiovascular_disease_deaths': row.cardiovascular_disease_deaths,
                'frequent_mental_distress': row.frequent_mental_distress,
                'lead_exposure_risk_index': row.lead_exposure_risk_index,
                'opioid_overdose_deaths': row.opioid_overdose_deaths,
                'smoking': row.smoking,
                'diabetes': row.diabetes,
                'obesity': row.obesity,
                'life_expectancy': row.life_expectancy,
                'limited_access_to_healthy_foods': row.limited_access_to_healthy_foods,
                'walkability': row.walkability
            }
            cities.append(city)
        session.close()

        return cities

    def get_city_health_ids(self, health_ids):
        session = Session()

        try:
            query_result = session.query(Health).filter(Health.id.in_(health_ids)).all()
            if query_result is None:
                session.close()
                abort(404)

            cities = []
            for row in query_result:
                city = {
                    'city': self.get_corresponding_city(row.city_id),
                    'air_pollution_particle': row.air_pollution_particle,
                    'cardiovascular_disease_deaths': row.cardiovascular_disease_deaths,
                    'frequent_mental_distress': row.frequent_mental_distress,
                    'lead_exposure_risk_index': row.lead_exposure_risk_index,
                    'opioid_overdose_deaths': row.opioid_overdose_deaths,
                    'smoking': row.smoking,
                    'diabetes': row.diabetes,
                    'obesity': row.obesity,
                    'life_expectancy': row.life_expectancy,
                    'limited_access_to_healthy_foods': row.limited_access_to_healthy_foods,
                    'walkability': row.walkability
                }
                cities.append(city)
            session.close()

            return cities
        except Exception as error:
            print(error)
            session.close()
            abort(400)

    def get_health_by_city(self, param_value):
        connection = self.engine.connect()

        table = Table('health', Base.metadata, autoload=True)
        city_id = self.get_corresponding_city_id(param_value)
        stmt = select([table]).where(table.c.city_id == city_id)

        try:
            query_result = connection.execute(stmt)

            if query_result is None:
                connection.close()
                abort(404)

            health = []
            for row in query_result:
                sanitize_data = {
                    'city': param_value,
                    'air_pollution_particle': row[2],
                    'cardiovascular_disease_deaths': row[3],
                    'frequent_mental_distress': row[4],
                    'lead_exposure_risk_index': row[5],
                    'opioid_overdose_deaths': row[6],
                    'smoking': row[7],
                    'diabetes': row[8],
                    'obesity': row[9],
                    'life_expectancy': row[10],
                    'limited_access_to_healthy_foods': row[11],
                    'walkability': row[12]
                }
            health.append(sanitize_data)
            connection.close()

            return health
        except Exception as error:
            connection.close()
            print(error)
            abort(400)

    def get_city_health_by_filter(self, attribute, param_value):
        connection = self.engine.connect()

        table = Table('health', Base.metadata, autoload=True)

        condition = attribute.split("_")[-1]
        stmt = None
        if condition == 'lt':
            attribute = attribute[0:len(attribute) - 3]
            stmt = select([table]).where(table.c[attribute] < param_value)
        if condition == 'gt':
            attribute = attribute[0:len(attribute) - 3]
            stmt = select([table]).where(table.c[attribute] > param_value)

        print(attribute)
        print(stmt)

        try:
            query_result = connection.execute(stmt)

            health_city = []
            for row in query_result:
                health = {
                    'city': self.get_corresponding_city(row[1]),
                    'air_pollution_particle': row[2],
                    'cardiovascular_disease_deaths': row[3],
                    'frequent_mental_distress': row[4],
                    'lead_exposure_risk_index': row[5],
                    'opioid_overdose_deaths': row[6],
                    'smoking': row[7],
                    'diabetes': row[8],
                    'obesity': row[9],
                    'life_expectancy': row[10],
                    'limited_access_to_healthy_foods': row[11],
                    'walkability': row[12]
                }
                health_city.append(health)
            connection.close()

            return health_city
        except Exception as error:
            connection.close()
            print(error)
            abort(400)

    def get_corresponding_city_id(self, city_name):
        try:
            connection = self.engine.connect()

            table = Table('cities', Base.metadata, autoload=True)
            stmt = select([table]).where(column('city') == city_name)
            query_result = connection.execute(stmt)
            city = None
            for row in query_result:
                city = row[0]
            connection.close()

            return city
        except Exception as error:
            print(error)
            connection.close()

            abort(404)

    def get_corresponding_city(self, city_id):
        try:
            connection = self.engine.connect()

            table = Table('cities', Base.metadata, autoload=True)
            stmt = select([table]).where(column('id') == city_id)
            query_result = connection.execute(stmt)

            city = None
            for row in query_result:
                city = row[1]
            connection.close()

            return city
        except Exception as error:
            connection.close()

            print(error)
            abort(404)
