import json

from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
import boto3
import requests

host = 'search-danger-zone-lhh2co2rietlqhpylh4w4wc3nm.us-east-1.es.amazonaws.com' # For example, my-test-domain.us-east-1.es.amazonaws.com
region = 'us-east-1' # e.g. us-west-1

service = 'es'
credentials = boto3.Session().get_credentials()
awsauth = AWS4Auth(credentials.access_key, credentials.secret_key, region, service, session_token=credentials.token)

es = Elasticsearch(
    hosts = [{'host': host, 'port': 443}],
    http_auth = awsauth,
    use_ssl = True,
    verify_certs = True,
    connection_class = RequestsHttpConnection
)
#
# with open('../assets/cities.json', 'r') as cities_file:
#     cities = json.loads(cities_file.read())
#     for city in cities:
#         document = city
#         es.index(index="cities", doc_type="_doc", body=document)
#
# with open('../assets/climate.json', 'r') as climate_file:
#     climates = json.loads(climate_file.read())
#     for climate in climates:
#         document = climate
#         es.index(index="climate", doc_type="_doc", body=document)
#
# with open('../assets/health.json', 'r') as health_file:
#     healths = json.loads(health_file.read())
#     for health in healths:
#         document = health
#         es.index(index="health", doc_type="_doc", body=document)
#
# with open('../assets/crimes.json', 'r') as crime_file:
#     crimes = json.loads(crime_file.read())
#     for crime in crimes:
#         document = crime
#         es.index(index="crime", doc_type="_doc", body=document)



res = es.search(index="cities", body={"query": {"match": {"city":"Austin"}}})
print(res)

