import requests
import json
import os

# documentation for the census gov API can be found at:
# https://www.census.gov/data/developers/guidance/api-user-guide.Query_Examples.html

"""
After base-url:
    year                        2019
    dataset name’s acronym      pep/population
    
    ?get=<variable,seaparated,by,commas&there_can_be_more_params
    &for=state:*
    &key=<secret_key>
    
    Example:
    https://api.census.gov/data/2014/pep/natstprc?get=STNAME,POP&DATE_=7&for=state:*&key=<your key here>
# https://www.census.gov/data/developers/data-sets/popest-popproj/popest.html
    
"""

CENSUS_SECRET_KEY = os.getenv('CENSUS_SECRET_KEY')
BASE_URL = "https://api.census.gov/data/"


# our data set example
# see https://api.census.gov/data/2019/pep/population/variables.html for more variables
# https://api.census.gov/data/2019/pep/population?get=POP,GEO_ID&for=state:*&key=<secret key>
def main():
    # complete_url variable to store
    # complete url address
    complete_url = "https://api.census.gov/data/2019/pep/population?get=POP,GEO_ID&for=state:*&key=" + CENSUS_SECRET_KEY

    # get method of requests module
    # return response object
    response = requests.get(complete_url)
    print(response)

    # json method of response object
    # convert json format data into
    # python format data
    json_obj = response.json()

    print(json_obj)


if __name__ == '__main__':
    main()
