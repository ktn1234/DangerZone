import requests
import json
import csv
import os

# documentation for the Cith Health API can be found at:
# https://api.cityhealthdashboard.com/

CITY_HEALTH_SECRET_KEY = os.getenv('CITY_HEALTH_SECRET_KEY')
BASE_URL = "https://api.cityhealthdashboard.com/api/data/city-metric/"

def request_data_for_metric_with_city(metric, city_name, state_abbreviation):
    params = {
        "token": CITY_HEALTH_SECRET_KEY,
        "city_name": city_name,
        "state_abbr": state_abbreviation
    }
    response = requests.get(BASE_URL + metric + "?", params=params)
    location_health = response.json()
    return location_health

#health metrics defined by API
health_metrics = [
    'air-pollution-particulate-matter', #avg daily of fine particulate matter per cubic meter
    'cardiovascular-disease-deaths', #deaths per 100,000
    'frequent-mental-distress', #% mental health not good for half month
    'lead-exposure-risk-index', #risk of lead exposure index
    'opioid-overdose-deaths', #per 100,000
    'smoking', #smoked >= 100 cigarettes and smoke frequently
    'diabetes', #adults who report their physical health not good for 14+ days
    'obesity', #obesity among adults
    'life-expectancy', #depends on current mortality rate
    'limited-access-to-healthy-foods', #% living .5 miles away from large grocery store
    'walkability' #index of amenities that are walk accessible
]

#health metrics exactly as listed in csv
health_metrics_csv = [
    'Air pollution - particulate matter',
    'Cardiovascular disease deaths',
    'Frequent mental distress',
    'Lead exposure risk index',
    'Opioid overdose deaths',
    'Smoking',
    'Diabetes',
    'Obesity',
    'Life expectancy',
    'Limited access to healthy foods',
    'Walkability'
]

all_states = {}

#abbreviations for states
all_abbrev = {
    'Alabama': 'AL',
    'Alaska': 'AK',
    'American Samoa': 'AS',
    'Arizona': 'AZ',
    'Arkansas': 'AR',
    'California': 'CA',
    'Colorado': 'CO',
    'Connecticut': 'CT',
    'Delaware': 'DE',
    'District of Columbia': 'DC',
    'Florida': 'FL',
    'Georgia': 'GA',
    'Guam': 'GU',
    'Hawaii': 'HI',
    'Idaho': 'ID',
    'Illinois': 'IL',
    'Indiana': 'IN',
    'Iowa': 'IA',
    'Kansas': 'KS',
    'Kentucky': 'KY',
    'Louisiana': 'LA',
    'Maine': 'ME',
    'Maryland': 'MD',
    'Massachusetts': 'MA',
    'Michigan': 'MI',
    'Minnesota': 'MN',
    'Mississippi': 'MS',
    'Missouri': 'MO',
    'Montana': 'MT',
    'Nebraska': 'NE',
    'Nevada': 'NV',
    'New Hampshire': 'NH',
    'New Jersey': 'NJ',
    'New Mexico': 'NM',
    'New York': 'NY',
    'North Carolina': 'NC',
    'North Dakota': 'ND',
    'Northern Mariana Islands':'MP',
    'Ohio': 'OH',
    'Oklahoma': 'OK',
    'Oregon': 'OR',
    'Pennsylvania': 'PA',
    'Puerto Rico': 'PR',
    'Rhode Island': 'RI',
    'South Carolina': 'SC',
    'South Dakota': 'SD',
    'Tennessee': 'TN',
    'Texas': 'TX',
    'Utah': 'UT',
    'Vermont': 'VT',
    'Virgin Islands': 'VI',
    'Virginia': 'VA',
    'Washington': 'WA',
    'West Virginia': 'WV',
    'Wisconsin': 'WI',
    'Wyoming': 'WY'
}

cities = ['austin', 'houston', 'dallas']
#for city in cities:

#    city_data = {}

#    for metric in health_metrics:
#        city_data[metric] = request_data_for_metric_with_city(metric, city, "TX")
#
#    with open(city + '_health.json', 'w') as f:
#        json.dump(city_data, f)


def main():
    #load in all cities
    all_cities = []
    with open('../assets/cities.json') as f:
        cities = json.load(f)
        if cities:
            print("Loaded cities data correctly")

        for city in cities:
            all_cities.append(city['City'])


    #build health dashboard
    health_dashboard = {}
    with open('../../../../api/CHDB_data_city_all v8_1TOT.csv') as csv_file:
        reader = csv.DictReader(csv_file)
        if reader:
            print("Loaded health dashboard reader.")
        n = 0
        for row in reader:
            city_name = row['city_name']
            met = row['metric_name']

            if met not in health_metrics_csv:
                continue

            if city_name in health_dashboard:
                city = health_dashboard[city_name]
            else:
                city = {}
            city['City'] = city_name
            city[met] = row['est']
            health_dashboard[city_name] = city
            print("Finished entry for " + city_name)
    print("Finished making health dashboard")

    #store city data
    for city in all_cities:
        #Darn slash is messing up script. slash is interpreted as directory
        if city == 'Louisville/Jefferson County':
            continue
        if city in health_dashboard:
            with open(city + '_health.json', 'w') as f:
                json.dump(health_dashboard[city], f, indent=4)
            print("Popular city added")
        else:
            cityEmpty = {
                'City': city,
                'Air pollution - particulate matter': -999,
                'Cardiovascular disease deaths': -999,
                'Frequent mental distress': -999,
                'Lead exposure risk index': -999,
                'Opioid overdose deaths': -999,
                'Smoking': -999,
                'Diabetes': -999,
                'Obesity': -999,
                'Life expectancy': -999,
                'Limited access to healthy foods': -999,
                'Walkability': -999
            }
            with open(city + '_health.json', 'w') as f:
                            json.dump(cityEmpty, f, indent=4)
            print("Empty city added")

    print("Done!")

if __name__ == '__main__':
    main()
# AVAILABLE METRICS TO SEARCH
"""
absenteeism
air-pollution-particulate-matter
binge-drinking
breast-cancer-deaths
cardiovascular-disease-deaths
children-in-poverty
colorectal-cancer-deaths
dental-care
diabetes
frequent-mental-distress
frequent-physical-distress
high-blood-pressure
high-school-graduation
housing-cost-excessive
housing-with-potential-lead-risk
income-inequality
lead-exposure-risk-index
life-expectancy
limited-access-to-healthy-foods
low-birthweight
neighborhood-racial-ethnic-segregation
obesity
opioid-overdose-deaths
park-access
physical-inactivity
premature-deaths-all-causes
prenatal-care
preventive-services
racial-ethnic-diversity
smoking 
teen-births
third-grade-reading-proficiency
unemployment
uninsured
violent-crime
walkability
"""