import os
import json


def main():
    climate_city_json = []
    entries = os.listdir('../assets/climate/climate_data_cleaned')
    for entry in entries:
        with open('../assets/climate/climate_data_cleaned/' + entry, 'r') as climate_city_file:
            data = climate_city_file.read()
            climate_json = json.loads(data)
            print(climate_json)
            climate_json['average_ozone'] = ((climate_json['spring']['ozone'] + climate_json['winter']['ozone'] + climate_json['autumn']['ozone'] + climate_json['summer']['ozone']) / 4)
            climate_city_json.append(climate_json)

    with open('../assets/climate.json', 'w') as climate_json_final:
        climate_json_final.write(json.dumps(climate_city_json))

    print(json.dumps(climate_city_json))


if __name__ == '__main__':
    main()