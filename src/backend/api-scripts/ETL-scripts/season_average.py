import json
import os
import itertools


def add(season, to_data, from_data):
    to_data[season]["temperature"] = to_data[season]["temperature"] + from_data["temperature"]
    to_data[season]["humidity"] = to_data[season]["humidity"] + from_data["humidity"]
    to_data[season]["uv"] = to_data[season]["uv"] + from_data["uvIndex"]
    to_data[season]["ozone"] = to_data[season]["ozone"] + from_data["ozone"]
    to_data[season]["windSpeed"] = to_data[season]["windSpeed"] + from_data["windSpeed"]


def divide(season, to_data, num):
    to_data[season]["temperature"] = to_data[season]["temperature"] / num
    to_data[season]["humidity"] = to_data[season]["humidity"] / num
    to_data[season]["uv"] = to_data[season]["uv"] / num
    to_data[season]["ozone"] = to_data[season]["ozone"] / num
    to_data[season]["windSpeed"] = to_data[season]["windSpeed"] / num


for filename in os.listdir("climate_data"):
    if (not filename.endswith(".json")):
        continue
    input = open("climate_data/" + filename, "r")
    print("HERE COMES THE CITY NAME")
    city_name = filename.split('_')[0]
    print(city_name)

    output = open(filename, "w")
    print(input)
    data = json.load(input)

    hourly_data = []
    for day in data.items():
        time = int(day[0])
        hours = day[1]["hourly"]["data"]
        for hour in hours:
            hourly_data.append({"data": hour, "time": time})

    # the math here converts seconds to season
    hourly_data = itertools.groupby(hourly_data, key=lambda daily: ((daily["time"] + 2628000) // 7884000) % 4)

    city_data = {
        "city": city_name,
        "winter": {
            "temperature": 0,
            "humidity": 0,
            "uv": 0,
            "ozone": 0,
            "windSpeed": 0
        },
        "spring": {
            "temperature": 0,
            "humidity": 0,
            "uv": 0,
            "ozone": 0,
            "windSpeed": 0
        },
        "summer": {
            "temperature": 0,
            "humidity": 0,
            "uv": 0,
            "ozone": 0,
            "windSpeed": 0
        },
        "autumn": {
            "temperature": 0,
            "humidity": 0,
            "uv": 0,
            "ozone": 0,
            "windSpeed": 0
        }
    }

    winters = 0
    springs = 0
    summers = 0
    autumns = 0

    for season, season_dailies in hourly_data:

        season_dailies = list(season_dailies)
        for season_daily in season_dailies:
            season_daily = season_daily["data"]
            if season == 0:
                winters = winters + 1
                add("winter", city_data, season_daily)
            elif season == 1:
                springs = springs + 1
                add("spring", city_data, season_daily)

            elif season == 2:
                summers = summers + 1
                add("summer", city_data, season_daily)

            elif season == 3:
                autumns = autumns + 1
                add("autumn", city_data, season_daily)

    divide("winter", city_data, winters)
    divide("spring", city_data, springs)
    divide("summer", city_data, summers)
    divide("autumn", city_data, autumns)

    output.write(json.dumps(city_data, indent=4))

    input.close()
    output.close()
