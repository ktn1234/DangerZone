import os
import json


def populate_health_city_json():
    health_city_json = []
    entries = os.listdir('../assets/health/health_data')
    for entry in entries:
        with open('../assets/health/health_data/' + entry, 'r') as health_city_file:
            data = health_city_file.read()
            health_json = json.loads(data)
            # print(health_json)
            health_city_json.append(health_json)

    with open('../assets/health.json', 'w') as health_json_final:
        health_json_final.write(json.dumps(health_city_json))

    print(json.dumps(health_city_json))


if __name__ == '__main__':
    populate_health_city_json()
