import json


def main():
    with open('../assets/cities.json', 'r') as cities_file:
        cities_data = cities_file.read()

    with open('../assets/climate.json', 'r') as climate_file:
        climate_data = climate_file.read()

    with open('../assets/crimes.json', 'r') as crimes_file:
        crime_data = crimes_file.read()

    with open('../assets/health.json', 'r') as health_file:
        health_data = health_file.read()

    # parse file
    cities = json.loads(cities_data)
    climates = json.loads(climate_data)
    crimes = json.loads(crime_data)
    health_city = json.loads(health_data)

    for city in cities:
        city['city'] = str(city['city']).lower()
        city['state'] = str(city['state']).lower()

    for climate in climates:
        climate['city'] = str(climate['city']).lower()

    for crime in crimes:
        crime['city'] = str(crime['city']).lower()

    for health in health_city:
        health['city'] = str(health['city']).lower()

    cities_file = open("cities.json", "w")
    climates_file = open("climate.json", "w")
    crimes_file = open("crimes.json", "w")
    health_file = open("health.json", "w")

    cities_file.write(json.dumps(cities))
    climates_file.write(json.dumps(climates))
    crimes_file.write(json.dumps(crimes))
    health_file.write(json.dumps(health_city))


if __name__ == '__main__':
    main()
