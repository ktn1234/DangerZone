import json


def main():
    with open('../assets/health.json', 'r') as health_file:
        health_data = health_file.read()

    # parse file
    health_city = json.loads(health_data)

    for health in health_city:
        if health['air_pollution_particle'] != -1:
            health['air_pollution_particle'] = float(health['air_pollution_particle'])

        if health['cardiovascular_disease_deaths'] != -1:
            health['cardiovascular_disease_deaths'] = float(health['cardiovascular_disease_deaths'])

        if health['frequent_mental_distress'] != -1:
            health['frequent_mental_distress'] = float(health['frequent_mental_distress'])

        if health['lead_exposure_risk_index'] != -1:
            health['lead_exposure_risk_index'] = float(health['lead_exposure_risk_index'])

        if health['opioid_overdose_deaths'] != -1:
            health['opioid_overdose_deaths'] = float(health['opioid_overdose_deaths'])

        if health['smoking'] != -1:
            health['smoking'] = float(health['smoking'])

        if health['diabetes'] != -1:
            health['diabetes'] = float(health['diabetes'])

        if health['obesity'] != -1:
            health['obesity'] = float(health['obesity'])

        if health['life_expectancy'] != -1:
            health['life_expectancy'] = float(health['life_expectancy'])

        if health['limited_access_to_healthy_foods'] != -1:
            health['limited_access_to_healthy_foods'] = float(health['limited_access_to_healthy_foods'])

        if health['walkability'] != -1:
            health['walkability'] = float(health['walkability'])
        print(health)

    file = open("health.json", "w")
    file.write(json.dumps(health_city))
    print(json.dumps(health_city))


if __name__ == '__main__':
    main()
