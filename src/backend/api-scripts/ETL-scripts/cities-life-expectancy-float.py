import json


def main():

    with open('../assets/cities.json', 'r') as cities_file:
        cities_data = cities_file.read()

    # parse file
    cities = json.loads(cities_data)

    for city in cities:
        if city['life_expectancy'] == "":
            city['life_expectancy'] = -1
        else:
            city['life_expectancy'] = float(city['life_expectancy'])

    file = open("cities.json", "w")
    file.write(json.dumps(cities))
    print(json.dumps(cities))


if __name__ == '__main__':
    main()