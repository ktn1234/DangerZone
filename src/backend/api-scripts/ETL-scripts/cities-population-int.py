import json


def main():

    with open('../assets/cities.json', 'r') as cities_file:
        cities_data = cities_file.read()

    # parse file
    cities = json.loads(cities_data)

    for city in cities:
        city['population'] = int(city['population'])

    file = open("cities.json", "w")
    file.write(json.dumps(cities))
    print(json.dumps(cities))


if __name__ == '__main__':
    main()