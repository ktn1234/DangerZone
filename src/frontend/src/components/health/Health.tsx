import React, {Component} from 'react'
import "./Health.css"
import {
    Route,
    Switch
} from "react-router-dom";

import {RouteComponentProps} from 'react-router';
import ReactTooltip from 'react-tooltip';

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Pagination from "react-bootstrap/Pagination";
import {Button} from "react-bootstrap";
let city_images = require('../../assets/cities/city_images.json');


interface HealthInstanceProps {
    deck: HealthObject[]
}

interface HealthInstanceState {
    deck: HealthObject[]
}

const metrics = [
    'air_pollution_particle', //avg daily of fine particulate matter per cubic meter
    'cardiovascular_disease_deaths', //deaths per 100,000
    'frequent_mental_distress', //% mental health not good for half month
    'lead_exposure_risk_index', //risk of lead exposure index
    'opioid_overdose_deaths', //per 100,000
    'smoking', //smoked >= 100 cigarettes and smoke frequently
    'diabetes', //adults who report their physical health not good for 14+ days
    'obesity', //obesity among adults
    'life_expectancy', //depends on current mortality rate
    'limited_access_to_healthy_foods', //% living .5 miles away from large grocery store
    'walkability' //index of amenities that are walk accessible
];

const metricDescriptions = [
    'Average daily amount of fine particulate matter per cubic meter.',
    'Number of deaths per 100,000 people.',
    'Percent of people with mental health issues for at least half a month.',
    'Risk of lead exposure index.',
    'Number of deaths per 100,000 people.',
    'Number of people who smoke frequently.',
    'Number of adults who report that their physical health is not good for at leas 14 days.',
    'Percentage of obese adults.',
    'Number of years average person lives. Depends on current mortality rate.',
    'Percentage of people living 0.5 miles from a large grocery store.',
    'Index of amenities that are walk accessible.'
];

const metricsSanitizedName: {[index: string]: string} = {
    'air_pollution_particle': 'Air pollution - particulate matter',
    'cardiovascular_disease_deaths': 'Cardiovascular disease deaths',
    'frequent_mental_distress': 'Frequent mental distress',
    'lead_exposure_risk_index': 'Lead exposure risk index',
    'opioid_overdose_deaths': 'Opioid overdose deaths',
    'smoking':'Smoking',
    'diabetes': 'Diabetes',
    'obesity': 'Obesity',
    'life_expectancy': 'Life expectancy',
    'limited_access_to_healthy_foods': 'Limited access to healthy foods',
    'walkability': 'Walkability'
};



function sanitizeName(metric: string): string {
    return metricsSanitizedName[metric];
}

interface HealthObject {
    'air_pollution_particle': number;
    'cardiovascular_disease_deaths': number;
    'frequent_mental_distress':number;
    'lead_exposure_risk_index':number;
    'opioid_overdose_deaths':number;
    'smoking':number;
    'diabetes':number;
    'obesity':number;
    'life_expectancy':number;
    'city':string;
    'limited_access_to_healthy_foods':number;
    'walkability':number;
}

class InfoImage extends Component {
    render() {
        return (
            <img className="info-tooltip" src={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTeG3ScOf71CRvMI54puvN-4zm_P_vNmQ0btzYmTTTUZ4NY66Rn&usqp=CAU"} height="18" width="18"/>
        )
    }
}

class HealthInstance extends Component<HealthInstanceProps & RouteComponentProps, HealthInstanceState> {
    constructor(props: any) {
        super(props);
        this.state = {
            deck: this.props.deck
        }

        this.checkForDeck = this.checkForDeck.bind(this)

    }

    componentDidMount(): void {
        console.log("instance mounted")
        fetch("https://api.dangerzone.life/health?city=" + (this.props.match.params as any).name)
            .then(res => res.json())
            .then(
                (result) => {
                    console.log("mounted instance: got the result")
                    this.setState({
                        deck: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )
    }

    componentDidUpdate(prevProps: Readonly<HealthInstanceProps & RouteComponentProps>, prevState: Readonly<HealthInstanceState>, snapshot?: any): void {
        console.log("instance updated")

    }

    checkForDeck() {
        this.setState(prevState => {
                return {
                    deck: this.props.deck
                }
            }
        )
    }


    render() {
        let item = (this.props.match.params as any).name;
        console.log("health instance: item is " + item)
        let data: any = null
        if(this.state.deck.length !== 0) {
            //iterate to correct city to display
            for(let i = 0; i < 9; i ++) {
                console.log("In for..." + this.state.deck[i]["city"])
                if (this.state.deck[i]["city"] === item)
                {
                    data = this.state.deck[i]
                    console.log("health instance: data is " + data)
                    break
                }
            }
        }
        return (
            <div className={"d-flex justify-content-center"}>
                <div>
                    <div className={"d-flex justify-content-center"}>
                        <a href={"/cities/" + item} role="button"><h3>{String(item).toUpperCase()}</h3></a>
                    </div>
                    <div>
                        {sanitizeName(metrics[0])}
                        <a data-tip={metricDescriptions[0]} data-for="attribute"><InfoImage/></a>
                        : {data && (data[metrics[0]] === -1 ? "No Data" : data[metrics[0]])}
                    </div>
                    <div>
                        {sanitizeName(metrics[1])}
                        <a data-tip={metricDescriptions[1]} data-for="attribute"><InfoImage/></a>
                        : {data && (data[metrics[1]] === -1 ? "No Data" : data[metrics[1]])}
                    </div>
                    <div>
                        {sanitizeName(metrics[2])}
                        <a data-tip={metricDescriptions[2]}  data-for="attribute"><InfoImage/></a>
                        : {data && (data[metrics[2]] === -1 ? "No Data" : data[metrics[2]])}
                    </div>
                    <div>
                        {sanitizeName(metrics[3])}
                        <a data-tip={metricDescriptions[3]}  data-for="attribute"><InfoImage/></a>
                        : {data && (data[metrics[3]] === -1 ? "No Data" : data[metrics[3]])}
                    </div>
                    <div>
                        {sanitizeName(metrics[4])}
                        <a data-tip={metricDescriptions[4]}  data-for="attribute"><InfoImage/></a>
                        : {data && (data[metrics[4]] === -1 ? "No Data" : data[metrics[4]])}
                    </div>
                    <div>
                        {sanitizeName(metrics[5])}
                        <a data-tip={metricDescriptions[5]}  data-for="attribute"><InfoImage/></a>
                        : {data && (data[metrics[5]] === -1 ? "No Data" : data[metrics[5]])}
                    </div>
                    <div>
                        {sanitizeName(metrics[6])}
                        <a data-tip={metricDescriptions[6]}  data-for="attribute"><InfoImage/></a>
                        : {data && (data[metrics[6]] === -1 ? "No Data" : data[metrics[6]])}
                    </div>
                    <div>
                        {sanitizeName(metrics[7])}
                        <a data-tip={metricDescriptions[7]}  data-for="attribute"><InfoImage/></a>
                        : {data && (data[metrics[7]] === -1 ? "No Data" : data[metrics[7]])}
                    </div>
                    <div>
                        {sanitizeName(metrics[8])}
                        <a data-tip={metricDescriptions[8]}  data-for="attribute"><InfoImage/></a>
                        : {data && (data[metrics[8]] === -1 ? "No Data" : data[metrics[8]])}
                    </div>
                    <div>
                        {sanitizeName(metrics[9])}
                        <a data-tip={metricDescriptions[9]}  data-for="attribute"><InfoImage/></a>
                        : {data && (data[metrics[9]] === -1 ? "No Data" : data[metrics[9]])}
                    </div>
                    <div>
                        {sanitizeName(metrics[10])}
                        <a data-tip={metricDescriptions[10]}  data-for="attribute"><InfoImage/></a>
                        : {data && (data[metrics[10]] === -1 ? "No Data" : data[metrics[10]])}
                    </div>

                    <ReactTooltip id="attribute" effect="solid" getContent={(dataTip) => <div>{dataTip}</div>}></ReactTooltip>
                </div>
            </div>
        )
    }

}

class Health extends Component<{}, {currentPage: number, healthDeck: HealthObject[], sort: string}> {
    constructor(props: any) {
        super(props);
        this.state = {
            currentPage: 0,
            healthDeck: [],
            sort: "true"
        }
        console.log("constructor: state is " + this.state.currentPage)
        console.log("constructor: healthDeck is " + this.state.healthDeck)

        this.chooseHand = this.chooseHand.bind(this)
    }

    sortChange(ascending: string) {

        let newSort = ""
        if(ascending === "true") {
            newSort = "true"
        } else {
            newSort = "false"
        }

        fetch("https://api.dangerzone.life/health?offset=0" + "&limit=9&column=smoking&asc=" + ascending)
            .then(res => res.json())
            .then(
                result => {
                    this.setState({
                        currentPage: 0,
                        healthDeck: result,
                        sort: newSort
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                error => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )
    }

    componentDidMount() {
        console.log("component did mount")
        console.log("mount:page is " + this.state.currentPage)
        this.chooseHand(this.state.currentPage)
        console.log("finish mounting")
    }



    chooseHand(pageNum: number) {
        console.log("you clicked " + pageNum)
        let finalPage = pageNum
        if (finalPage < 0) {
            finalPage = 0
        }

        //Only 100 instances, 9 displayed, so 12 pages total
        if (finalPage > 10) {
            finalPage = 10
        }

        const itemsToDisplay = 9
        let offset = finalPage * itemsToDisplay

        fetch("https://api.dangerzone.life/health?offset=" + offset + "&limit=9&column=smoking&asc=" + this.state.sort)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        currentPage: finalPage,
                        healthDeck: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )

        this.setState(prevState => {
                return {
                    currentPage: finalPage
                }
            }
        )
    }



    render() {
        let itemsToDisplay = 9
        if (this.state.healthDeck.length === 0) {
            itemsToDisplay = 0
        }

        let sample_cities: string[] = new Array(itemsToDisplay)
        console.log("render: health deck length is " + this.state.healthDeck.length)
        console.log("render: healthDeck is " + this.state.healthDeck)

        for (let i = 0; i < itemsToDisplay; i++) {
            //let offset = i + this.state.currentPage * itemsToDisplay
            //Don't read undefineds in all_data
            //if (all_data[offset]) {
            //    sample_cities[i] = all_data[offset]["city"]
            //}
            if (this.state.healthDeck) {
                sample_cities[i] = this.state.healthDeck[i]['city']
            }

        }

        //Populate random images for cities without image
        for (let i = 0; i < itemsToDisplay; i++) {
            if ((typeof(city_images[sample_cities[i]]) === "undefined") || (city_images[sample_cities[i]] === "")) {
                city_images[sample_cities[i]] = ("https://placeimg.com/640/480/arch?t=" + Math.random() * Math.pow(10,9))
            }
        }


        let id = 0;
        console.log("sample cities are " + sample_cities)
        const citiesHealthClickables = sample_cities.map((city) => {
            const cityInfo = this.state.healthDeck[id];
            const lifeExpectancy = cityInfo.life_expectancy;
            const air_pollution_particle = cityInfo.air_pollution_particle;
            const cardiovascular_disease_deaths = cityInfo.cardiovascular_disease_deaths;


            return (
                <div key={id++} className="col-md-3 m-3">
                    <div className="d-flex justify-content-center">
                        <h2>{String(city).toUpperCase()}</h2>
                    </div>
                    <a href={"/health/" + city} role="button">
                        <img className="city-img" src={city_images[city]} height={"100"}/>
                    </a>
                    <div className="d-flex justify-content-center">
                        <b>Life Expectancy: {lifeExpectancy === -1 ? "No Data" : lifeExpectancy}</b>
                    </div>
                    <div className="d-flex justify-content-center">
                        <b>Air Pollution Particle: {air_pollution_particle === -1 ? "No Data" : air_pollution_particle}</b>
                    </div>
                    <div className="d-flex justify-content-center">
                        <b>Cardiovascular Disease: {cardiovascular_disease_deaths === -1 ? "No Data" : cardiovascular_disease_deaths}</b>
                    </div>
                </div>
            );
        });

        return (
            <div className={"container"}>

                <Switch>
                    <Route path="/health/:name" render={(props) =>
                        <HealthInstance {...props} deck={this.state.healthDeck} />
                    }/>
                    <Route path="/health" exact render={() => {
                        return (
                            <div>
                                <Button variant="outline-danger">Sort by % of Smokers </Button>
                                <Button variant="danger" active={this.state.sort === "true"}
                                        onClick={() => this.sortChange("true")}>Ascending</Button>
                                <Button variant="danger" active={this.state.sort === "false"}
                                        onClick={() => this.sortChange("false")}>Descending</Button>
                                <div className="row d-flex justify-content-center">
                                    {citiesHealthClickables}
                                </div>
                                <Pagination className={"justify-content-center"}>
                                    <Pagination.First onClick={() => this.chooseHand(0)}/>
                                    <Pagination.Prev onClick={() => this.chooseHand(this.state.currentPage-1)}/>
                                    <Pagination.Item active={true} onClick={() => this.chooseHand(this.state.currentPage-1)}>{this.state.currentPage}</Pagination.Item>
                                    <Pagination.Next onClick={() => this.chooseHand(this.state.currentPage+1)}/>
                                    <Pagination.Last onClick={() => this.chooseHand(10)}/>
                                </Pagination>
                            </div>
                        )
                    }}/>

                </Switch>


            </div>
        );
    }
}

export default Health
