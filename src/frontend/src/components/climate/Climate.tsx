import React, { Component } from 'react'
import {
    Route,
    Switch
} from "react-router-dom";

import { RouteComponentProps } from 'react-router';

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import ListGroup from "react-bootstrap/ListGroup";
import ListGroupItem from "react-bootstrap/ListGroupItem";
import Pagination from "react-bootstrap/Pagination";
import {Button} from "react-bootstrap";


let timestamp = require('unix-timestamp');
let austin_data = require('../../assets/climate_weather/austin_weather.json');
let houston_data = require('../../assets/climate_weather/houston_weather.json');
let dallas_data = require('../../assets/climate_weather/dallas_weather.json');

let city_images = require('../../assets/cities/city_images.json');


interface ClimateObject {
    average_ozone: number;
    city: string;
    fall: SeasonObject;
    spring: SeasonObject;
    summer: SeasonObject;
    winter: SeasonObject;

}

interface SeasonObject {
    humidity: number;
    ozone: number;
    temperature: number;
    'uv': number;
    'windSpeed': number;
}

const metrics = [
    'humidity',
    'ozone',
    'temperature',
    'uv',
    'windSpeed'
]

interface ClimateInstanceProps {
    deck: ClimateObject[]
}

interface ClimateInstanceState {
    deck: ClimateObject[]
}

class ClimateInstance extends Component<ClimateInstanceProps & RouteComponentProps, ClimateInstanceState> {
    constructor(props: any) {
        super(props);
        this.state = {
            deck: this.props.deck
        }

        this.checkForDeck = this.checkForDeck.bind(this)

    }

    componentDidMount(): void {
        console.log("instance mounted")
        fetch("https://api.dangerzone.life/climate?city=" + (this.props.match.params as any).name)
            .then(res => res.json())
            .then(
                (result) => {
                    console.log("mounted instance: got the result")
                    this.setState({
                        deck: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )
    }

    componentDidUpdate(prevProps: Readonly<ClimateInstanceProps & RouteComponentProps>, prevState: Readonly<ClimateInstanceState>, snapshot?: any): void {
        console.log("instance updated")

    }

    checkForDeck() {
        this.setState(prevState => {
                return {
                    deck: this.props.deck
                }
            }
        )
    }

    render() {

        let item = (this.props.match.params as any).name;
        console.log("climate instance: item is " + item)

        const emptySeason: SeasonObject = {
            humidity: -1,
            ozone: -1,
            temperature: -1,
            uv: -1,
            windSpeed: -1,
        }

        const empty: ClimateObject = {
            average_ozone: -1,
            city: 'none',
            fall: emptySeason,
            spring: emptySeason,
            summer: emptySeason,
            winter: emptySeason,
        }

        let data: ClimateObject = empty

        if(this.state.deck.length > 0) {
            //iterate to correct city to display
            for(let i = 0; i < 9; i ++) {
                if (this.state.deck[i]["city"] === item)
                {
                    data = this.state.deck[i]
                    console.log("climate instance: data is " + data['city'])
                    break
                }
            }
        }

        return (
            <div className={"d-flex justify-content-center"}>
                <div>
                    <div className={"d-flex justify-content-center"}>
                    <a href={"/cities/" + item} role="button">
                        <h3>{String(item).toUpperCase()}</h3></a>
                    </div>
                    <ListGroup>
                        <ListGroupItem>
                            <h4>Spring</h4>
                            <p className={"ml-3"}>
                                Avg Temperature: {data && data['spring']['temperature'].toFixed(2) + " °F"} <br/>
                                UV Index: {data && data['spring']['uv'].toFixed(2) + " "} <br/>
                                Humidity: {data && data['spring']['humidity'].toFixed(2) + " "} <br/>
                                Ozone: {data && data['spring']['ozone'].toFixed(2) + " "} <br/>
                                Wind Speed: {data && data['spring']['windSpeed'].toFixed(2) + " "} <br/>
                            </p>
                        </ListGroupItem>
                        <ListGroupItem>
                            <h4>Summer</h4>
                            <p className={"ml-3"}>
                                Avg Temperature: {data && data['summer']['temperature'].toFixed(2) + " °F"} <br/>
                                UV Index: {data && data['summer']['uv'].toFixed(2) + " "} <br/>
                                Humidity: {data && data['summer']['humidity'].toFixed(2) + " "} <br/>
                                Ozone: {data && data['summer']['ozone'].toFixed(2) + " "} <br/>
                                Wind Speed: {data && data['summer']['windSpeed'].toFixed(2) + " "} <br/>
                            </p>
                        </ListGroupItem>
                        <ListGroupItem>
                            <h4>Fall</h4>
                            <p className={"ml-3"}>
                                Avg Temperature: {data && data['fall']['temperature'].toFixed(2) + " °F"} <br/>
                                UV Index: {data && data['fall']['uv'].toFixed(2) + " "} <br/>
                                Humidity: {data && data['fall']['humidity'].toFixed(2) + " "} <br/>
                                Ozone: {data && data['fall']['ozone'].toFixed(2) + " "} <br/>
                                Wind Speed: {data && data['fall']['windSpeed'].toFixed(2) + " "} <br/>
                            </p>
                        </ListGroupItem>
                        <ListGroupItem>
                            <h4>Winter</h4>
                            <p className={"ml-3"}>
                                Avg Temperature: {data && data['winter']['temperature'].toFixed(2) + " °F"} <br/>
                                UV Index: {data && data['winter']['uv'].toFixed(2) + " "} <br/>
                                Humidity: {data && data['winter']['humidity'].toFixed(2) + " "} <br/>
                                Ozone: {data && data['winter']['ozone'].toFixed(2) + " "} <br/>
                                Wind Speed: {data && data['winter']['windSpeed'].toFixed(2) + " "} <br/>
                            </p>
                        </ListGroupItem>

                    </ListGroup>

                </div>
            </div>
        )
    }
}

class Climate extends Component<{}, {currentPage: number, climateDeck: ClimateObject[], sort: string}> {
    constructor(props: any) {
        super(props);
        this.state = {
            currentPage: 0,
            climateDeck: [],
            sort: "true"
        }
        console.log("constructor: state is " + this.state.currentPage)
        console.log("constructor: healthDeck is " + this.state.climateDeck)

        this.chooseHand = this.chooseHand.bind(this)
    }

    sortChange(ascending: string) {

        let newSort = ""
        if(ascending === "true") {
            newSort = "true"
        } else {
            newSort = "false"
        }

        fetch("https://api.dangerzone.life/climate?offset=0" + "&limit=9&column=average_ozone&asc=" + ascending)
            .then(res => res.json())
            .then(
                result => {
                    this.setState({
                        currentPage: 0,
                        climateDeck: result,
                        sort: newSort
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                error => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )
    }

    componentDidMount() {
        console.log("component did mount")
        console.log("mount:page is " + this.state.currentPage)
        this.chooseHand(this.state.currentPage)
        console.log("finish mounting")
    }


    chooseHand(pageNum: number) {
        console.log("you clicked " + pageNum)
        let finalPage = pageNum
        if (finalPage < 0) {
            finalPage = 0
        }

        //Only 100 instances, 9 displayed, so 12 pages total
        if (finalPage > 10) {
            finalPage = 10
        }

        const itemsToDisplay = 9
        let offset = finalPage * itemsToDisplay

        fetch("https://api.dangerzone.life/climate?offset=" + offset + "&limit=9&column=average_ozone&asc=" + this.state.sort)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        currentPage: finalPage,
                        climateDeck: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log("error is " + error)
                    this.setState({});
                }
            )

        this.setState(prevState => {
                return {
                    currentPage: finalPage
                }
            }
        )
    }


    render() {
        let itemsToDisplay = 9
        if (this.state.climateDeck.length === 0) {
            itemsToDisplay = 0
        }

        let sample_cities: string[] = new Array(itemsToDisplay)
        console.log("render: health deck length is " + this.state.climateDeck.length)
        console.log("render: healthDeck is " + this.state.climateDeck)

        for (let i = 0; i < itemsToDisplay; i++) {
            //let offset = i + this.state.currentPage * itemsToDisplay
            //Don't read undefineds in all_data
            //if (all_data[offset]) {
            //    sample_cities[i] = all_data[offset]["city"]
            //}
            if (this.state.climateDeck) {
                sample_cities[i] = this.state.climateDeck[i]['city']
            }

        }

        //Populate random images for cities without image
        for (let i = 0; i < itemsToDisplay; i++) {
            if ((typeof(city_images[sample_cities[i]]) === "undefined") || (city_images[sample_cities[i]] === "")) {
                city_images[sample_cities[i]] = ("https://placeimg.com/640/480/arch?t=" + Math.random() * Math.pow(10,9))
            }
        }


        let id = 0;
        console.log("sample cities are " + sample_cities)
        const citiesClimateClickables = sample_cities.map((city) => {
            const cityInfo = this.state.climateDeck[id];
            const spring = cityInfo.spring.temperature.toPrecision(4);
            const summer = cityInfo.summer.temperature.toPrecision(4);
            const fall = cityInfo.fall.temperature.toPrecision(4);
            const winter = cityInfo.winter.temperature.toPrecision(4);

            return (<div key={id++} className="col-md-3 m-3">
                <div className="d-flex justify-content-center">
                    <h2>{String(city).toUpperCase()}</h2>
                </div>
                <a href={"/climate/" + city} role="button">
                    <img className="city-img" src={(city_images[city])} height={"100"}/>
                </a>
                <div className="d-flex justify-content-center">
                    <b>Spring Avg Temp: {spring}°F</b>
                </div>
                <div className="d-flex justify-content-center">
                    <b>Summer Avg Temp: {summer}°F</b>
                </div>
                <div className="d-flex justify-content-center">
                    <b>Fall Avg Temp: {fall}°F</b>
                </div>
                <div className="d-flex justify-content-center">
                    <b>Winter Avg Temp: {winter}°F</b>
                </div>
            </div>
        );
    });

        return (
            <div className={"container"}>

                <Switch>
                    <Route path="/climate/:name" render={(props) =>
                        <ClimateInstance {...props} deck={this.state.climateDeck}/>
                    }/>
                    <Route path="/climate" exact render={() => {
                        return (
                            <div>
                                <Button variant="outline-danger">Sort by Average Ozone </Button>
                                <Button variant="danger" active={this.state.sort === "true"}
                                        onClick={() => this.sortChange("true")}>Ascending</Button>
                                <Button variant="danger" active={this.state.sort === "false"}
                                        onClick={() => this.sortChange("false")}>Descending</Button>
                                <div className="row d-flex justify-content-center">
                                    {citiesClimateClickables}
                                </div>
                                <Pagination className={"justify-content-center"}>
                                    <Pagination.First onClick={() => this.chooseHand(0)}/>
                                    <Pagination.Prev onClick={() => this.chooseHand(this.state.currentPage - 1)}/>
                                    <Pagination.Item active={true}
                                                     onClick={() => this.chooseHand(this.state.currentPage - 1)}>{this.state.currentPage}</Pagination.Item>
                                    <Pagination.Next onClick={() => this.chooseHand(this.state.currentPage + 1)}/>
                                    <Pagination.Last onClick={() => this.chooseHand(10)}/>
                                </Pagination>
                            </div>
                        )
                    }}/>

                </Switch>


            </div>
        );
    }
}

export default Climate