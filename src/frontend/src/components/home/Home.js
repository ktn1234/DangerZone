import React, {Component} from 'react'
import {Row} from 'react-bootstrap'
import "./style.css"

class Home extends Component {
    render() {
        return (
            <div className="container text-center">
                    <div className={"fixed-top"}>
                        <img src={require("./Images/drawing-farmhouse-cityscape-transparent.png")}
                             className="img-responsive"
                             height={500}
                             width={"1650"}
                        />
                    </div>

                <div className={'frontPageInfo'}>
                    <h1 className={"text-dark"}>This is the Danger Zone.</h1>
                </div>

                <div className={'frontPageInfo1'}>
                    <h2 className="lead text-dark"> Informing people of dangers in each city.</h2>
                </div>

                <div className={'lets-get-started'}>
                    <a href={"/cities"}><button onClick={this.redirect} type="button" className="btn btn-success">Let's Get Started</button></a>
                </div>

                <Row>
                    <div className={"fixed-bottom"}>
                        <img src={require("./Images/drawing-farmhouse-cityscape-transparent.png")}
                             className="img-responsive"
                             height={500}
                             width={"1650"}
                        />
                    </div>
                </Row>
            </div>
        );
    }
}

export default Home