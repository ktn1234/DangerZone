import React, { Component } from 'react'
import {Bar, Doughnut} from 'react-chartjs-2';
import Container from "react-bootstrap/Container";

interface CityObject {
    state: string;
    population: number;
    time_zone: string;
    latitude: number;
    longitude: number;
    'average_high': number;
    'average_low': string;
    city: string;
    'life_expectancy': number;
    'average_ozone': number;
}

interface CrimeObject {
    animal_cruelty: number;
    assault: number;
    burglary:number;
    city:string;
    drugs:number;
    dui:number;
    murder:number;
    other_offenses:number;
    sex_offenses:number;
    theft:number;
    total_crime_incidents:number;
    vandalism:number;
}

interface ChartState {
    citylabels: Array<string>,
    citydata: Array<number>,
    crimelabels: Array<string>,
    crimedata: Array<number>
}

class Chart extends Component<{},ChartState>{
    constructor(props: any){
        super(props);
        this.state = {
            citylabels: [
                "cityName0",
                "cityName1",
                "cityName2",
                "cityName3",
                "cityName4",
                "cityName5",
                "cityName6",
                "cityName7",
                "cityName8",
                "cityName9",
            ],
            citydata: [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
            ],
            crimelabels: [
                "crimeName0",
                "crimeName1",
                "crimeName2",
                "crimeName3",
                "crimeName4",
                "crimeName5",
                "crimeName6",
                "crimeName7",
                "crimeName8",
                "crimeName9",
            ],
            crimedata: [
                10,
                10,
                10,
                10,
                10,
                10,
                10,
                10,
                10,
                10,
            ]
        }
    }

    readableName(input: string) {
        return input.replace("_", " ")
    }

    componentDidMount() {
        fetch("https://api.dangerzone.life/cities?offset=0&limit=9&column=population&asc=false")
            .then(res => res.json())
            .then(
                result => {
                    let labelStore = []
                    let dataStore = []
                    let cityList: CityObject[] = result
                    for (let index = 0; index < 9; index++) {
                        labelStore.push(cityList[index]["city"])
                        console.log("labels here are " + labelStore)
                        // @ts-ignore
                        dataStore.push(cityList[index]["population"])
                    }
                    this.setState({
                        citylabels: labelStore,
                        citydata: dataStore
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                error => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )

        fetch("https://api.dangerzone.life/crime?city=austin")
            .then(res => res.json())
            .then(
                result => {
                    let crimeLabelStore = []
                    let crimeDataStore = []
                    let crimeCityList: CrimeObject = result[0]
                    for (let key in crimeCityList) {
                        if (key !== "total_crime_incidents" && key !== "city") {
                            crimeLabelStore.push(this.readableName(key))
                            console.log("labels here are " + crimeLabelStore)
                            // @ts-ignore
                            crimeDataStore.push(crimeCityList[key])
                        }

                    }
                    this.setState({
                        crimelabels: crimeLabelStore,
                        crimedata: crimeDataStore
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                error => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )
    }

    getOptions(title: string) {
        return {
            title: {
                display: true,
                text: title,
                fontSize: 35,
            },
            legend: {
                labels: {
                    fontColor: "red",
                    fontSize: 22,
                },

            }
        }
    }

    getCityPop() {
        return {
            labels: this.state.citylabels,
            datasets: [
                {
                    label: "City Model",
                    backgroundColor: "#F44336",
                    data: this.state.citydata,
                },
            ],
        };
    }

    getCityCrimes() {
        return {
            labels: this.state.crimelabels,
            datasets: [
                {
                    label: "Crimes for One City Model",
                    backgroundColor: [
                        "#F39C12",
                        "#7DCEA0",
                        "#A569BD",
                        "#EC7063",
                        "#EB984E",
                        "#EBEDEF",
                        "#2E4053",
                        "#33691E",
                        "#B71C1C",
                        "#0033FF"
                    ],
                    data: this.state.crimedata,
                },
            ],
        };
    }

    render(){
        return (
            <div>
                <Bar
                    data={this.getCityPop()}
                    options={this.getOptions("Top 10 Populous Cities")}
                />

                <br/><hr/>

                <Doughnut data={this.getCityCrimes()}
                          options={this.getOptions("Austin Crimes by Category")}
                />


            </div>

        )
    }
}

export default Chart;