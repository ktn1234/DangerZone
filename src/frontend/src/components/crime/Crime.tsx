import React, { Component } from 'react'
import "./Crime.css"
import {RouteComponentProps} from 'react-router';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import {
    Route,
    Switch
} from "react-router-dom";
import {Simulate} from "react-dom/test-utils";
import Pagination from "react-bootstrap/Pagination";
import {stringify} from "querystring";
import DropdownButton from "react-bootstrap/DropdownButton";
import { Dropdown, Button } from 'react-bootstrap';
import DropdownMenu from "react-bootstrap/DropdownMenu";

let city_images = require('../../assets/cities/city_images.json');

const metrics = [
    "total_crime_incidents",
    "murder",
    "assault",
    "drugs",
    "burglary",
    "vandalism",
    "dui",
    "sex_offenses",
    "theft",
    "animal_cruelty",
    "other_offenses"
];

const metricsSanitizedName: {[index: string]: string} = {
    "total_crime_incidents": "Total Crime Incidents",
    "murder": "Murders",
    "assault": "Assaults",
    "drugs": "Drug-related Crimes",
    "burglary": "Burglaries",
    "vandalism": "Vandalism",
    "dui": "DUIs",
    "sex_offenses": "Sex Offenses",
    "theft": "Thefts",
    "animal_cruelty": "Animal Cruelty Cases",
    "other_offenses": "Other Offenses"
};

function sanitizeName(metric: string): string {
    return metricsSanitizedName[metric];
}

function capitalizeName(name: string): string {
    return name.charAt(0).toUpperCase() + name.substring(1);
}

interface CrimeObject {
    animal_cruelty: number;
    assault: number;
    burglary:number;
    city:string;
    drugs:number;
    dui:number;
    murder:number;
    other_offenses:number;
    sex_offenses:number;
    theft:number;
    total_crime_incidents:number;
    vandalism:number;
}

interface CrimeInstanceProps {
    chicken: string,
    deck: CrimeObject[]
}

interface CrimeInstanceState {
    deck: CrimeObject[]
}

class CrimeInstance extends Component<CrimeInstanceProps & RouteComponentProps, CrimeInstanceState> {

    constructor(props: any) {
        super(props);
        this.state = {
            deck: this.props.deck
        }

        this.checkForDeck = this.checkForDeck.bind(this)

    }

    componentDidMount(): void {
        console.log("instance mounted")
        fetch("https://api.dangerzone.life/crime?city=" + (this.props.match.params as any).name)
            .then(res => res.json())
            .then(
                result => {
                    console.log("mounted instance: got the result")
                    this.setState({
                        deck: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                error => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )
    }

    componentDidUpdate(prevProps: Readonly<CrimeInstanceProps & RouteComponentProps>, prevState: Readonly<CrimeInstanceState>, snapshot?: any): void {
        console.log("instance updated")
    }

    checkForDeck() {
        this.setState(prevState => {
                return {
                    deck: this.props.deck
                }
            }
        )
    }

    render() {
        console.log("crime instance: chicken is " + this.props.chicken)
        console.log("crime instance deck length is: " + this.state.deck.length)

        let item = (this.props.match.params as any).name;
        console.log("crime instance: item is " + item)
        let data: any = null
        if(this.state.deck.length !== 0) {
            //iterate to correct city to display
            for(let i = 0; i < 9; i ++) {
                if (this.state.deck[i]["city"] === item)
                {
                    data = this.state.deck[i]
                    break
                }
            }
        }



        console.log("crime instance: data is " + data)
        return <div>
            <div className={"d-flex justify-content-center"}>
                <div>
                    <div className={"d-flex justify-content-center"}>
                        <a href={"/cities/" + item} role="button"><h3>{String(item).toUpperCase()}</h3></a>
                    </div>
                    <p>{sanitizeName(metrics[0])}: {data && data[metrics[0]]}</p>
                    <p>{sanitizeName(metrics[1])}: {data && data[metrics[1]]}</p>
                    <p>{sanitizeName(metrics[2])}: {data && data[metrics[2]]}</p>
                    <p>{sanitizeName(metrics[3])}: {data && data[metrics[3]]}</p>
                    <p>{sanitizeName(metrics[4])}: {data && data[metrics[4]]}</p>
                    <p>{sanitizeName(metrics[5])}: {data && data[metrics[5]]}</p>
                    <p>{sanitizeName(metrics[6])}: {data && data[metrics[6]]}</p>
                    <p>{sanitizeName(metrics[7])}: {data && data[metrics[7]]}</p>
                    <p>{sanitizeName(metrics[8])}: {data && data[metrics[8]]}</p>
                    <p>{sanitizeName(metrics[9])}: {data && data[metrics[9]]}</p>
                    <p>{sanitizeName(metrics[10])}: {data && data[metrics[10]]}</p>


                </div>
            </div>

        </div>
    }
}

class Crime extends Component<{},{currentPage: number, crimesDeck: CrimeObject[], sort: string}>{

    constructor(props: any) {
        super(props);
        this.state = {
            currentPage: 0,
            crimesDeck: [],
            sort: "true"
        }

        console.log("constructor: state is " + this.state.currentPage)
        console.log("constructor: crimesDeck is " + this.state.crimesDeck)


        this.chooseHand = this.chooseHand.bind(this)
        this.sortChange = this.sortChange.bind(this)

    }

    sortChange(ascending: string) {

        let newSort = ""
        if(ascending === "true") {
            newSort = "true"
        } else {
            newSort = "false"
        }

        fetch("https://api.dangerzone.life/crime?offset=0" + "&limit=9&column=murder&asc=" + ascending)
            .then(res => res.json())
            .then(
                result => {
                    this.setState({
                        currentPage: 0,
                        crimesDeck: result,
                        sort: newSort
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                error => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )
    }

    componentDidMount() {
        console.log("component did mount")
        console.log("mount:page is " + this.state.currentPage)
        this.chooseHand(this.state.currentPage)
        console.log("finish mounting")
    }

    chooseHand(pageNum: number) {
        console.log("you clicked " + pageNum)
        let finalPage = pageNum
        if (finalPage < 0) {
            finalPage = 0
        }

        //Only 100 instances, 9 displayed, so 12 pages total
        if (finalPage > 10) {
            finalPage = 10
        }

        const itemsToDisplay = 9
        let offset = finalPage * itemsToDisplay

        fetch("https://api.dangerzone.life/crime?offset=" + offset + "&limit=9&column=murder&asc=" + this.state.sort)
            .then(res => res.json())
            .then(
                result => {
                    this.setState({
                        currentPage: finalPage,
                        crimesDeck: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                error => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )

        this.setState(prevState => {
                return {
                    currentPage: finalPage
                }
            }
        )
    }

    render() {
        let itemsToDisplay = 9
        if (this.state.crimesDeck.length === 0) {
            itemsToDisplay = 0
        }

        let sample_cities: string[] = new Array(itemsToDisplay)
        console.log("render: crimes deck length is " + this.state.crimesDeck.length)
        console.log("render: crimesDeck is " + this.state.crimesDeck)


        for (let i = 0; i < itemsToDisplay; i++) {
            //let offset = i + this.state.currentPage * itemsToDisplay
            //Don't read undefineds in all_data
            //if (all_data[offset]) {
            //    sample_cities[i] = all_data[offset]["city"]
            //}
            if (this.state.crimesDeck) {
                sample_cities[i] = this.state.crimesDeck[i]['city']
            }

        }

        //Populate random images for cities without image
        for (let i = 0; i < itemsToDisplay; i++) {
            if (typeof city_images[sample_cities[i]] === "undefined" || city_images[sample_cities[i]] === "") {
                city_images[sample_cities[i]] = "https://placeimg.com/640/480/arch?t=" + Math.random() * Math.pow(10, 9)
            }
        }

        //Create unique keys for map
        let id = 0;
        console.log("sample cities are " + sample_cities)
        const citiesCrimeClickables = sample_cities.map((city: string) => {
            const cityInfo = this.state.crimesDeck[id];
            const totalCrime = cityInfo.total_crime_incidents;
            const murder = cityInfo.murder;
            const assault = cityInfo.assault;

            return(<div key={id++} className="col-md-3 m-3">
                <div className="d-flex justify-content-center">
                    <h2>{String(city).toUpperCase()}</h2>
                </div>
                <a href={"/crime/" + city} role="button">
                    <img className="city-img" src={city_images[city]} height={"100"}/>
                </a>
                <div className="d-flex justify-content-center">
                    <b>Total Crime Count: {totalCrime === 0 ? "No Data" : totalCrime}</b>
                </div>
                <div className="d-flex justify-content-center">
                    <b>Murder: {murder === 0 ? "No Data" : murder}</b>
                </div>
                <div className="d-flex justify-content-center">
                    <b>Assault: {assault === 0 ? "No Data" : assault}</b>
                </div>
            </div>
        );
    });

        return (
            <div className={"container"}>
            <Switch>
                <Route path="/crime/:name" render={
                    props =>
                        <CrimeInstance {...props} chicken={'you got the chicken'} deck={this.state.crimesDeck}/>
                }/>
                <Route path="/crime" exact render={() => {
                    return <div>
                        <Button variant="outline-danger">Sort by Total Crimes </Button>
                        <Button variant="danger" active={this.state.sort === "true"}
                                onClick={() => this.sortChange("true")}>Ascending</Button>
                        <Button variant="danger" active={this.state.sort === "false"}
                                onClick={() => this.sortChange("false")}>Descending</Button>
                        <div className="row d-flex justify-content-center">
                            {citiesCrimeClickables}
                        </div>

                        <Pagination className={"justify-content-center"}>
                            <Pagination.First onClick={() => this.chooseHand(0)}/>
                            <Pagination.Prev onClick={() => this.chooseHand(this.state.currentPage - 1)}/>
                            <Pagination.Item active={true}
                                             onClick={() => this.chooseHand(this.state.currentPage - 1)}>{this.state.currentPage}</Pagination.Item>
                            <Pagination.Next onClick={() => this.chooseHand(this.state.currentPage + 1)}/>
                            <Pagination.Last onClick={() => this.chooseHand(10)}/>
                        </Pagination>
                    </div>
                }}/>
            </Switch>
            </div>
        );
    }
}

export default Crime