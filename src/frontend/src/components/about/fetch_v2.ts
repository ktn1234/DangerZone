import fetch from "node-fetch";

const projectId = 16914165;

const unitTests: Array<number> = [
    0,  // Kevin
    23,  // Vincent
    14,  // Andy
    0,  // Vy
    0   // Preston
];

const memberIds: Array<number> = [
    5109816,
    5355817,
    5365237,
    5321308,
    5442996
];

const names: Array<string> = [
    "Kevin Nguyen",
    "Vincent Li",
    "Andy Wang",
    "Vy Phan",
    "Preston Brown"
];

export interface MemberStats {
    id: number;
    name: string;
    commitsMade: number;
    issuesClosed: number;
    unitTests: number;
}

function mapToCorrectName(name: string) {
    const dict: {[index: string]: string} = {
        "Kevin Nguyen": "Kevin Nguyen",
        "Vincent Li": "Vincent Li",
        "Vy Phan": "Vy Phan",
        "Andy Wang": "Andy Wang",
        "Preston Brown": "Preston Brown",
        "phbprogramming": "Preston Brown",
        "pokecrater1": "Vy Phan",
        "KontosTwo": "Vincent Li",
        "andywang6666": "Andy Wang",
        "vphan6896": "Vy Phan",
        "ktn1234": "Kevin Nguyen",
        "unknown": "Vy Phan"
    };

    return dict[name];
}

async function populateGroupMembers(): Promise<Array<MemberStats>> {
    const groupMembers: Array<MemberStats> = new Array<MemberStats>();
    for(let i = 0; i < memberIds.length; i++) {
        const member: MemberStats = {
            id: memberIds[i],
            name: names[i],
            commitsMade: 0,
            issuesClosed: 0,
            unitTests: unitTests[i]
        };
        groupMembers.push(member);
    }

    return groupMembers;
}

export async function getGitlabStats(): Promise<Array<MemberStats>> {
    const groupMembers: Array<MemberStats> = await populateGroupMembers();

    // parse all issues closed by group members
    // https://gitlab.com/api/v4/projects/16914165/issues?state=closed&page=<1-100>
    for(let i = 1; i <= 10; i++) {
        const closedIssues = await fetch(`https://gitlab.com/api/v4/projects/${projectId}/issues?state=closed&page=${i}`)
            .then(async (res: any) => {
                return await res.json();
            })
            .catch((err: any) => console.error(err));

        // console.log(closedIssues.length);
        if(closedIssues.length === 0) {
            break;
        }

        for(let j = 0; j < closedIssues.length; j++) {
            // console.log(closedIssues[j]["closed_by"]["name"]);
            groupMembers.find(member => {
                if(closedIssues[j].state === 'closed') {
                    if (member.name === closedIssues[j]["closed_by"]["name"]) {
                        member.issuesClosed++;
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    // https://gitlab.com/api/v4/projects/16914165/repository/commits?page=<1-100>
    for(let i = 1; i <= 10; i++) {
        const commits = await fetch(`https://gitlab.com/api/v4/projects/${projectId}/repository/commits?page=${i}`)
            .then(async (res: any) => {
                return await res.json();
            })
            .catch((err: any) => console.error(err));

        // console.log(commits);
        if(commits.length === 0) {
            break;
        }

        for(let j = 0; j < commits.length; j++) {
            groupMembers.find(member => {
                if(mapToCorrectName(commits[j]['committer_name']) === member.name) {
                    member.commitsMade++;
                    return true;
                }
                return false;
            });
        }
    }

    console.log(groupMembers);
    return groupMembers;
}

interface IssuesStatistics {
    all: string;
    closed: string;
    opened: string;
}

// https://gitlab.com/api/v4/projects/16914165/issues_statistics
export async function getIssuesStats(): Promise<IssuesStatistics> {
    const issuesStats = await fetch(`https://gitlab.com/api/v4/projects/${projectId}/issues_statistics`)
        .then(async (res: any) => {
            return await res.json();
        })
        .catch((err: any) => console.error(err));

    const issuesStatistics: IssuesStatistics = {
        all: issuesStats['statistics']['counts']['all'],
        closed: issuesStats['statistics']['counts']['closed'],
        opened: issuesStats['statistics']['counts']['opened']
    };

    console.log(issuesStatistics);

    return issuesStatistics
}

// https://gitlab.com/api/v4/projects/16914165/repository/commits?page=<1-10>
export async function getTotalCommits(): Promise<number> {
    let totalCommits: number = 0;

    for(let i = 1; i <= 10; i++) {
        const commits = await fetch(`https://gitlab.com/api/v4/projects/${projectId}/repository/commits?page=${i}`)
            .then(async (res: any) => {
                return await res.json();
            })
            .catch((err: any) => console.error(err));

        if(commits.length === 0) {
            break;
        } else {
            totalCommits += commits.length;
        }
    }

    console.log(totalCommits);

    return totalCommits;
}

// Useful API calls
// https://gitlab.com/api/v4/projects/16914165/repository/contributors
// https://gitlab.com/api/v4/projects/16914165/repository/commits?page=5
// https://gitlab.com/api/v4/projects/16914165/members
async function troubleShootGitlabStats() {
    const groupMembers: Array<MemberStats> = await populateGroupMembers();

    const commits = await fetch(`https://gitlab.com/api/v4/projects/${projectId}/repository/commits?page=6`)
        .then(async (res: any) => {
            return await res.json();
        })
        .catch((err: any) => console.error(err));

    for(let j = 0; j < commits.length; j++) {
        groupMembers.find(member => {
            if(mapToCorrectName(commits[j]['committer_name']) === member.name) {
                member.commitsMade++;
                return true;
            }
            return false;
        });
    }

    console.log(groupMembers);
    return groupMembers;
}
