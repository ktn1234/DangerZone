# CS 373 Project:

#### Members:

1. Kevin Nguyen / kn8586 / ktn1234
2. Vincent Li / vl5545 / KontosTwo
3. Preston Brown / phb385 / phbprogramming
4. Andy Wang / arw3262 / andywang6666
5. Vy Phan / vp6896 / vphan6896

<br/>

Git SHA: 38bb3cf20f3922689f598888c3429123443fa1d7

<br/>

Project Leader: Kevin Nguyen

<br/>

Link to GitLab pipelines: https://gitlab.com/ktn1234/DangerZone/pipelines

<br/>

Link to website: https://www.dangerzone.life

<br/>

Link to Postman Documentation: https://documenter.getpostman.com/view/10517580/SzKYPGyC?version=latest

<br/>

Estimated completion time for each member (hours: int):

|      | Andy | Vy | Vincent | Kevin | Preston |
|------|------|----|---------|-------|---------|
| IDB1 | 20   | 20 | 20      | 20    | 20      |
| IDB2 | 30   | 30 | 30      | 30    | 30      |
| IDB3 | 25   | 25 | 25      | 25    | 25      |

Actual completion time for each member (hours: int):

|      | Andy | Vy | Vincent | Kevin | Preston |
|------|------|----|---------|-------|---------|
| IDB1 | 23   | 25 | 18      | 25    | 31      |
| IDB2 | 20   | 30 | 20      | 40    | 0       |
| IDB3 | 17   | 25 | 20      | 25    | 0       |

<br/>

Comments: N/A
